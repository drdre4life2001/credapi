@include('home.navbar')

@include('home.sidebar')

     <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom CSS -->
   <style>
    /*!
 * Start Bootstrap - Simple Sidebar HTML Template (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

/* Toggle Styles */

#wrapper {
    padding-left: 0;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}
.grey {
    background-color:rgba(128, 128, 128, 0.28); 
}
#wrapper.toggled {
    padding-left: 250px;
}
.sidebar-nav > li {
     color:white;
     font-family: Hind;
}
.mtop {
    margin-top: -20px;
}

.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
    color: #555;
    background-color: #4B78A7;
}
#sidebar-wrapper {
    z-index: 1000;
    position: fixed;
    left: 250px;
    width: 0;
    height: 100%;
    margin-left: -250px;
    overflow-y: auto;
    background: #000;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}
.bold {
    font-weight: bold;
}
#wrapper.toggled #sidebar-wrapper {
    width: 250px;
}

#page-content-wrapper {
    width: 100%;
    position: absolute;
    padding: 15px;
}

#wrapper.toggled #page-content-wrapper {
    position: absolute;
    margin-right: -250px;
}

/* Sidebar Styles */

.sidebar-nav {
    position: absolute;
    top: 0;
    width: 250px;
    margin: 0;
    padding: 0;
    list-style: none;
}

.sidebar-nav li {
    text-indent: 20px;
    line-height: 40px;
}

.sidebar-nav li a {
    display: block;
    text-decoration: none;
    color: #999999;
}

.sidebar-nav li a:hover {
    text-decoration: none;
    color: #fff;
    background: rgba(255,255,255,0.2);
}

.sidebar-nav li a:active,
.sidebar-nav li a:focus {
    text-decoration: none;
}

.sidebar-nav > .sidebar-brand {
    height: 65px;
    font-size: 18px;
    line-height: 60px;
}

.sidebar-nav > .sidebar-brand a {
    color: #999999;
}

.sidebar-nav > .sidebar-brand a:hover {
    color: #fff;
    background: none;
}

@media(min-width:768px) {
    #wrapper {
        padding-left: 250px;
    }

    #wrapper.toggled {
        padding-left: 0;
    }

    #sidebar-wrapper {
        width: 250px;
    }

    #wrapper.toggled #sidebar-wrapper {
        width: 0;
    }

    #page-content-wrapper {
        padding: 20px;
        position: relative;
    }

    #wrapper.toggled #page-content-wrapper {
        position: relative;
        margin-right: 0;
    }
}
    </style>
<div id="page-content-wrapper" class="" style="overflow-x:hidden; margin-top:0%; padding-top:0%">
            <div class="container-fluid">
                <div class="row" style="width:90%; margin-left:20%">
                    <div class="col-sm-6" style="width:90%" >
                        <h1>Create Order</h1>
                        <!-- <input type="button" href="#menu-toggle" class="btn btn-default" id="menu-toggle" onclick="change()" value="Close Side Menu" /> -->
                        <br/><hr/>
                        <p>This method creates the order and setup rapayment based based on customer payment  option "POST".<br/>
                      
                        <br/>
                        </p>
                       <hr/>
                       <h3>Endpoint URL</h3>
                       <p>https://api.credpal.com/api/create-order</p>
                       <hr/>
                       <h3>Request parameters</h3>
                       <div class="container col-lg-12 " style=" width:200%;border-top:2px solid mar grey;border-bottom:2px solid grey;border-left:5px solid grey;border-right:1px solid grey">
                        <div class="row grey">
                            <div class="col-md-4 bold">
                            <h6>order_description:</h6>
                            </div>
                            <div class="col-md-8 " >
                            <h4></h4>Samsung galaxy j5</h6>
                            </div>
                        </div>
                      <div class="row">
                            <div class="col-md-4 bold">
                            <h6>order_price:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>12000</h6>
                            </div>
                        </div>
                        <div class="row grey">
                            <div class="col-md-4 bold">
                            <h6>order_equity:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>0</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 bold">
                            <h6>order_status:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>0</h6>
                            </div>
                        </div>
                        <div class="row grey">
                            <div class="col-md-4 bold">
                            <h6>order_user_id:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>109</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 bold">
                            <h6>order_lender_id:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>1</h6>
                            </div>
                        </div>
                        <div class="row grey">
                            <div class="col-md-4 bold">
                            <h6>order_source:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>1</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 bold">
                            <h6>order_lender_drop:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>1</h6>
                            </div>
                        </div>
                        <div class="row grey">
                            <div class="col-md-4 bold">
                            <h6>order_date:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>2019-2-6</h6>
                            </div>
                        </div>

                        <div class="row grey">
                            <div class="col-md-4 bold">
                            <h6>order_merchant_id:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>1</h6>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-4 bold">
                            <h6>order_email:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>you@example.com</h6>
                            </div>
                        </div>   
                        <div class="row grey">
                            <div class="col-md-4 bold">
                            <h6>monthly_repayment:</h6>
                            </div>
                            <div class="col-md-8 bold" >
                            <h4></h4> <h6>3</h6>
                            </div>
                        </div>   
                               
                        <div class="row">
                            <div class="col-md-3 bold">
                           
                        </div>
                        </div>
                    </div>

                        </div>
                    </div>
                 <div class=""style="width:50%; margin-left:20%">
                     <br>
                    <h3>Response</h3>
                    <ul>
                    <li>Success format : <br/>
                    <code style=""><pre>      
                    {
    "success": true,
    "data": {
        "order_user_id": "1",
        "order_description": "samsung galaxy j5",
        "order_price": "20000",
        "order_lender_id": "1",
        "order_source": "1",
        "order_drop": "1",
        "order_date": 1221422460,
        "order_equity": "1200",
        "order_merchant_id": "1",
        "order_status": "0",
        "order_hash": "5c5c6dc57568d",
        "updated_at": "2019-02-07 17:41:25",
        "created_at": "2019-02-07 17:41:25",
        "id": 26
    },
    "message": "Order was created and repayment was setup successfully"
}</pre></code> </li>
      <li>Failure Response : <br/>
      <code><pre>    {
    "success": false,
    "message": "Not Found.",
    "data": "Eamil does not exist"
}</pre></code>
      </li>
      </ul>
                 </div>

                   <!--      <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a> -->
                    </div>
                </div>
            </div>
        </div>
     
        <script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Menu Toggle Script -->
<script>
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
</script>

        <script type="text/javascript">
                    function change() {
            var elem = document.getElementById('menu-toggle');
             if (elem.value=="Close Side Menu") elem.value = "Open Side Menu";
            else elem.value = "Close Side Menu";
        }
        </script>
        <script>
            $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    