<div id="page-content-wrapper" class="mtop pside" style="overflow-x:hidden">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                    <h1>Getting Started with Credpal API</h1>
                       <p>This page will help you get started with Credpal API.</p>
                      <hr/>
                       <p>This API allows for merchants to easily leverage on the credpal system to allow their customers access credit for orders on their platform:</p>
                       <ul>
                       <li>Customers can pay on installment for order purchases </li>
                       <li>Customers can be verified and and credited instantly</li>
                       <li>Customers can access credpal system for the after the checkout</li>
                                             </ul>
                       <p>The REST API can be used to:</p>
                         <ul>
                       <li>Create and verify customer for credit worthiness</li>
                       <li>Verify customer payment</li>
                       <li>Upload customer bank statement</li>


                        </ul>
                   <!--      <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a> -->
                    </div>
                </div>
            </div>
        </div>  