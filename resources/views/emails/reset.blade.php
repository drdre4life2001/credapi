<!DOCTYPE html>
<html>
<head>
   <title>email template</title>
</head>
<body>
   <div>
      <div>
         <span style="display:none;font-size:0px;line-height:0px;max-width:0px;min-height:0px">Welcome to CredPal
            <img style="margin:0 0.2ex;vertical-align:middle;max-height:24px"> 
         </span>
         <center>
            <table style="border-collapse:collapse;min-height:100%;margin:0;padding:0;width:100%;background-color:#edeff9" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
               <tbody>
                  <tr>
                     <td style="min-height:100%;margin:0;padding:10px;width:100%;border-top:0" valign="top" align="center">
                        <table style="border-collapse:collapse;border-color:currentcolor;border-style:none;border-width:0px;max-width:600px!important" width="100%" cellspacing="0" cellpadding="0" border="0">
                           <tbody>

                              <tr>
                                 <td style="background-image:none;background-color:#fff;background-repeat:no-repeat;background-size:cover;border-top:0px none;border-bottom:0px none;padding-top:10px;padding-bottom:10px" valign="top">
                                    <table style="min-width:100%;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                                       <tbody>
                                          <tr>
                                             <td style="padding:9px" valign="top">
                                                <table style="min-width:100%;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0" align="middle">
                                                   <tbody>
                                                      <tr>
                                                         <td style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align: center;" valign="top">
                                                            <a href="https://credpal.com" target="_blank">
                                                               <img alt="CredPal Logo" src="https://credpal.com/Logo/logo-dark.png" style="max-width:150px;padding-bottom:0px;display:inline;vertical-align:bottom;border-color:currentcolor;border-style:none;border-width:0px;outline:currentcolor none medium;text-decoration:none;width:139px">
                                                            </a>
                                                         </td>
                                                      </tr>
                                                     
                                                      <tr></tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>

                              <tr>
                                 <td style="background-image:url('https://credpal.com/mails/email.jpg');background-repeat:no-repeat;background-size:cover;border-top:0px none;border-bottom:0px none;padding-top:100px;padding-bottom:100px" valign="top">
                                    
                                 </td>
                              </tr>

                              <tr>
                                 <td style="background-color:rgb(255,255,255);background-image:none;background-repeat:no-repeat;background-size:cover;border-top:0px none;border-bottom:2px solid rgb(234,234,234);padding-top:0px;padding-bottom:9px" valign="top">
                                    <table style="min-width:100%;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                                       <tbody>
                                          <tr>
                                             <td style="padding-top:9px" valign="top">
                                                <table style="max-width:100%;min-width:100%;border-collapse:collapse"width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                   <tbody>
                                                      <tr>
                                                         <td style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#525252;font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:22px;text-align:left;font-weight: 400" valign="top">
                                                            <br>
                                                            Hello ,
                                                            
                                                            <p style="margin:10px 0;padding:0;color:#525252;font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:22px;text-align:left">
                                                            Please click blow to reset your password
                                                            </p>
                                                            <p>
                                                            <a class="btn btn-info"href="{{$file_path}}">here</a>
                                                            </p>
                                                            <br>
                                                            
                                                            <p style="margin:10px 0;padding:0;color:#525252;font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:22px;text-align:left">
                                                               Once again, welcome and happy shopping.
                                                            </p>
                                                            <br>
                                                            LIFE IS NOW, LIVE MORE!
                                                         </td>
                                                      </tr>
                                                      <tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                          <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                                             <tbody class="m_4642015061396928607ydpbdb35eacyiv0730069643mcnCaptionBlockOuter">
                                                <tr>
                                                   <td style="padding:9px" valign="top">
                                                      <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                         <tbody>
                                                            <tr>
                                                               <td style="background-color:#777777;background-image:none;background-repeat:no-repeat;background-size:cover;border-top:0px none;border-bottom:0px none;padding-top:9px;padding-bottom:9px" valign="top">
                                                                  <table style="min-width:100%;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="padding:9px" valign="top">
                                                                              <table style="min-width:100%;
                                                                              order-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:center" valign="top">
                                                                                       <div style="max-width:750px;padding-bottom:0px;display:inline;vertical-align:bottom;border-color:currentcolor;border-style:none;border-width:0px;outline:currentcolor none medium;text-decoration:none;width:600px" align="middle" tabindex="0">
                                                                                          <p style="color: #fff;padding: 0;font-family:Helvetica,Arial,sans-serif;font-size:15px;line-height:22px;padding-left: 18px">Follow Us</p>
                                                                                       </div>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>

                                                               <table style="min-width:100%;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td style="padding-top:9px" valign="top">
                                                                           <table style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#ffffff;font-family:'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:12px;line-height:150%;text-align:center" valign="top">

                                                                                       <span style="display: inline-block;"><a style="padding: 10px 15px;border:1px solid #fff;color: #fff;border-radius: 100%;cursor:pointer;margin-right: 10px;text-decoration: none" target="_blank">f</a></span>

                                                                                       <span style="display: inline-block;"><a href="https://twitter.com/credpal?lang=en" style="padding: 10px 15px;border:1px solid #fff;color: #fff;border-radius: 100%;cursor:pointer;margin-right: 10px;text-decoration: none" target="_blank">t</a>
                                                                                       </span>

                                                                                        <span style="display: inline-block;"><a href="https://ng.linkedin.com/company/credpal" style="padding: 10px 12px;border:1px solid #fff;color: #fff;border-radius: 100%;cursor:pointer;text-decoration: none" target="_blank">in</a></span>
                                                                                       

                                                                                       
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>

                                                               <table style="min-width:100%;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td style="padding-top:9px" valign="top">
                                                                           <table style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#ffffff;font-family:'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:12px;line-height:150%;text-align:center" valign="top">
                                                                                       <em>Copyright &copy; {{date('Y')}}, All rights reserved.</em>
                                                                                       <br>
                                                                                       <br>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </center>

                                 </div>

                              </div>
                           </body>
                           </html>