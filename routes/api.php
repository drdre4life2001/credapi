<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
// API register
Route::post('register', 'API\RegisterController@register');

Route::middleware(['auth:api','cors'])->group(function () {
  //  Route::post('get-userprofile','API\UserProfileController@fetchUserProfile');

  //userlogin and password reset
    Route::post('request-password-reset', 'API\CredpalUserController@passwordResetRequest');
    Route::post('set-password/{token}', 'API\CredpaluserController@setPassword');
    Route::post('password-update', 'API\CredpalUserController@updatePassword');
    Route::post('user-login', 'API\CredpalUserController@userlogin');
    Route::get('viewuser', 'API\CredpalUserController@viewprofile');
    
    //user registration
    Route::post('user-register', 'API\CredpalUserController@userRegister');

    Route::post('get-userprofile', 'API\UserProfileController@fetchUserProfile');
    Route::post('get-credit-limit', 'API\UserProfileController@fetchCreditLimit');
    Route::post('create-order', 'API\OrderController@store');
    Route::post('add-profle', 'API\UserProfileController@store');
    Route::post('verify-payment', 'API\PaymentController@verify');
    Route::post('automatic-verification', 'API\LoanEngineController@loanCheck');
    Route::post('manual-verification', 'API\LoanEngineController@upload');


});

Route::get('example', array('middleware' => 'cors', 'uses' => 'API\LoanEngineController@cors'));