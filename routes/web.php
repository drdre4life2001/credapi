<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('home.index');
});

Route::get('/create-profile', function () {
    return view('home.create-profile');
});
Route::get('/automatic-verification', function () {
    return view('home.automatic');
});
Route::get('/create-order', function () {
    return view('home.order');
});

Route::get('/card-setup', function () {
    return view('home.card-setup');
});

Route::get('/manual-verification', function () {
    return view('home.manual');
});
Route::get('/authentication', function () {
    return view('home.auth');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
