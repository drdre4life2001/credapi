<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Loan extends Model
{
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // public function comments()
    // {
    //     return $this->hasMany('App\Comment');
    // }
    protected $fillable = [
        'order_description', 'order_price', 'order_lender_id',
        'order_source','order_user_id','order_drop','order_date', 'order_merchant_id',
        'order_status', 'order_equity',    
    ];
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}