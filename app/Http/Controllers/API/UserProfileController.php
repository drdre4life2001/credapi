<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\UserProfile;
use App\User;
use App\Classes\Bvn;
use App\Classes\LoanCalculator;
use Validator;
use DB;
use App\Classes\SMS;
use Illuminate\Support\Facades\sendMaiAuth;
use Illuminate\Support\Facades\Storage;
use Mail;


class UserProfileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($bvn)
    {
        // $validator = Validator::make($details, [
        //     'name' => 'required',
        //     'email' => 'required|email',
        //     'phone' => 'required',
        //     'bvn' => 'reuired',
        //     'password' => 'min:6',
        //     'password_confirmation' => 'required_with:password|same:password|min:6'
        // ]);


    }

    public function fetchUserProfile(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'bvn' => 'required',
        ]);

        $bvn = $request->bvn;
        $user_profile = DB::table('user_profile')->where('up_bvn', $bvn)->first();

        if (empty($user_profile)) {

            return $this->sendError('No record', 'No profile with the bvn provided');

        } else {
            return $this->sendResponse($user_profile, 'Profile retrieved successfully .');

        }


    }

    public function fetchCreditLimit(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'bvn' => 'required',
        ]);
        $bvn = $request->bvn;

        $user_profile = DB::table('user_profile')->where('up_bvn', $bvn)->value('up_credit');

        if (empty($user_profile)) {
            return $this->sendError('No record', ' Credit limit is not set');
        } else {
            return $this->sendResponse($user_profile, 'Credit limit retrieved successfully .');

        }


    }

    /**
     * Store a newly created orders in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $details = $request->all();
        // print_r($details);
        // exit();
        $validator = Validator::make($details, [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'date_of_birth' => 'required|date',
            'gender' => 'required',
            'bvn' => 'required|digits:11',
            'address' => 'required',
            'state' => 'required',
            'lga' => 'required',
            'home_status' => 'required',
            'employment_status' => 'required',
            'hostmerchantid'=>'required'

        ]);


        if ($validator->fails()) {
            return $this->sendError('Please fill all required field.', $validator->errors());
        }

        // if ($details['state'] != 'lagos') {
        //     return $this->sendError('Only Lagos residents can and apply', $validator->errors());
        // }

        $email = DB::table('users')->where('email', $details['email'])->value('email');
        $checkbvn = new bvn();

        $checkbvn = $checkbvn->checkBvn($details['bvn']);

        if (array_key_exists('data', $checkbvn)) {

            if ($checkbvn['data']['bvn'] != $details['bvn'])
                return $this->sendError('Invalid bvn, please supply a valid bvn', $validator->errors());


       } elseif (!array_key_exists('data', $checkbvn)) {

            return $this->sendError('Invalid bvn, please supply a valid bvn', $validator->errors());

        }

        // if user already exists, don't crate user
        if ($email != $details['email']) {

      //BVN verification
           


            $default = uniqid();
            $default = bcrypt($default);
            $userid = DB::table('users')->where('email', $email)->value('id');
            $token = hash_hmac('sha256', str_random(40), config('app.key'));
            $userdata = array(
                'name' => $details['name'],
                'email' => $details['email'],
                'phone' => $details['phone'],
                'user_form_status' => 1,
                'otp' => rand(1000, 9999),
                'remember_token' => $token,
                'destination'=>'checkout',
                'hostmerchantid'=>$details['hostmerchantid'],
                'password' => $default
            );
            $user_details = User::create($userdata);

            $updateuser = DB::table('users')->where('id', $user_details->id)->update([
                'uniquehash' => uniqid().'-'.$user_details->id,
                'bvn'=>$details['bvn']
            ]);

            $sender = Mail::send('emails.welcome', ['data' => $details], function ($msg) use ($details) {
                $msg->from('hello@credpal.com', 'Credpal');

                $msg->to($details['email'], 'Credpal')->subject('Welocome');
            });

            $msg = 'Your Credpal account was created successfully. To  access your credit, visit credpal.com/forgot-password to set your password';
            $userid = DB::table('users')->where('email', $email)->value('id');
            $sms = new SMS;
            $userid = DB::table('users')->where('email', $email)->value('id');
            //$send = $sms->send_sms($details['phone'], $msg);
        }

        $userid = DB::table('users')->where('email', $email)->value('id');


        $bvn = DB::table('user_profile')->where('up_bvn', $details['bvn'])->value('up_bvn');
        $userid = DB::table('users')->where('email', $details['email'])->value('id');
        $checkemail = DB::table('users')->where('email', $details['email'])->get();

        // if bvn already exists  don't add to porfile
        if ($bvn != $details['bvn'] && count($checkemail) <= 0) {
            $data = array(
                'up_user_id' => $userid,
                'up_gender' => $details['gender'],
                'up_bvn' => $details['bvn'],
                'up_address' => $details['address'],
                'up_state' => $details['state'],
                'up_location' => $details['lga'],
                // 'up_city' => $details['employment_state'],
                'up_lga' => $details['lga'],
                'up_dob' => $details['date_of_birth'],
                'up_home_status' => $details['home_status'],
                'up_employment_status'=> $details['employment_status'], 
            );

            $user_profile = UserProfile::create($data);

         

            return $this->sendResponse($user_profile, 'Profile was created successfully.');


        } else {


            return $this->sendResponse($userid, 'Profile was created successfully.');

        }

    }



}