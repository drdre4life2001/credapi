<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Classes\AfricasTalkingGateway;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Classes\Dudu;
use App\Classes\Credit;
use App\Classes\CreditBureauEngine;


class LoanEngineController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    public function upload(Request $request)
    {
        $file_type =$request->file('statement')->getClientOriginalExtension();
        $size = $request->file('statement')->getClientSize();
        $file_name = $request->file('statement')->getClientOriginalName();
        $details = $request->all();

        $validator = Validator::make($details, [
            'statement' => 'required|file|mimes:pdf|max:20000',
            'email' =>'required|email'
        ]);
       

        if ($validator->fails()) {
            return $this->sendError('Please file type or size not allowed', $validator->errors());

        }else
        {
            $file = Storage::put($file_name,file_get_contents($details['statement']));
            $file_path = 'http://' . $_SERVER['SERVER_NAME']  . $file;
            
            $user_id = DB::table('users')->where('email', $details['email'])->value('id');
           
            if (empty($user_id)) {
                return $this->sendError('Not Found.', 'Eamil does not exist');
            }
            
            DB::table('files')->insert(array(
                'file_name' => $file_name,
                'file_path' => $file_path,
                'file_user_id' => $user_id,
                'file_size' => $size,
                'file_type' => 1,
                'file_created' => time()
            ));

            $updateusers = DB::table('users')->where('id', $user_id)->update([
                'verification_option' => 2
            ]);
           // Storage::disk('local')->put('file.txt', 'Contents');
           $store = Storage::put($file_name,file_get_contents($details['statement']));
        }
     
        if (isset($store)){

            return $this->sendResponse($details, 'file upload was successful.');

        }
    }


    /**
     * Store a newly created orders in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function loanCheck(Request $request)
	{	
        $details = $request->all();

        $validator = Validator::make($details, [
            'bvn' => 'required',
            'id'  => 'required'
        ]);
       
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
       
				//get crdit bureau analysis
				$getcredit = new CreditBureauEngine;
				$getresult = $getcredit->bureauAnalysis($details['bvn'], $details['id']);
				$response = json_decode($getresult, true);
              
				if ($response['status'] == 'fail' || $response['maximum'] == 0) {

					return $this->sendResponse('No Credit record found', ' Upload Bank Statement manually');
				}

           //check for viable credit history	
				if ($response['code'] == 11) {

					$maximum = $response['maximum'];

					$credit_limit = $maximum * 6;

					DB::table('user_profile')->where([
						['up_bvn', '=',$details['bvn']],
					])->update(array(
						'up_credit' => $credit_limit
					));
                    return $this->sendResponse($credit_limit, 'Credit limit is set');
                    $updateusers = DB::table('users')->where('id', $details['id'])->update([
                        'verification_option' => 1
                    ]);

                }
                else {
                    return $this->sendError(' Error.', 'Incorrect details supplied');
                }


    }
    public function cors(){
        return $this->sendError(' Error.', 'Fuck cors');
    }

    

}