<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\CredUser;
use DB;
use Mail;

class CredpalUserController extends BaseController
{

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function userlogin(Request $request)
    {
        $details = $request->all();
        $validator = Validator::make($details, [
            'email' => 'required',
            'password' => 'required'

        ]);

        $userid = DB::table('users')->where('email', $details['email'])->value('id');
        $user = CredUser::find($userid);
        $hasher = app('hash');
        if (empty($userid)) {

            return $this->sendError('Wrong credentials', 'Email or password incorrect');

        } elseif ($hasher->check($details['password'], $user->password)) {

            return $this->sendResponse($user, 'User logged in .');

        } else {
            return $this->sendError('Wrong credentials', 'Email or password incorrect');

        }

    }

    public function userRegister(Request $request)
    {

        $details = $request->all();
        $validator = Validator::make($details, [
            'full_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Please fill all required field.', $validator->errors());
        }
        $password = $details['c_password'];
        $password = bcrypt($password);
        $token = hash_hmac('sha256', str_random(40), config('app.key'));

        $userdata = array(
            'name' => $details['full_name'],
            'email' => $details['email'],
            'phone' => $details['phone'],
            'user_form_status' => 2,
            'user_status' => 1,
            'otp' => rand(1000, 9999),
            'userid' => uniqid(),
            'password' => $password,
            'reset_token' => $token,
        );
        $email = DB::table('users')->where('email', $details['email'])->value('email');
        if (!empty($email)) {
            return $this->sendError('You have an existing record on our system');
        }
        $user_details = CredUser::create($userdata);
        if ($user_details) {

            $sender = Mail::send('emails.welcome', ['data' => $details], function ($msg) use ($details) {
                $msg->from('hello@credpal.com', 'Credpal');

                $msg->to($details['email'], 'Credpal')->subject('Welcome');
            });

            return $this->sendResponse($user_details, 'Profile was created successfully.');
        } else {
            return $this->sendError('User not created', 'Please check your input and try again');
        }
    }

    public function passwordResetRequest(Request $request)
    {

        $details = $request->all();
        $validator = Validator::make($details, [

            'email' => 'required|email',
        ]);

        $email = DB::table('users')->where('email', $details['email'])->value('email');

        if (is_null($email)) {

            return $this->sendError('Invalid email. Please ensure your eamils is correct');

        } else {


            $token = DB::table('users')->where('email', $details['email'])->value('reset_token');
            $file_path = 'http://' . $_SERVER['SERVER_NAME'] . '/' . $token;

            // $sender = Mail::send('emails.reset', ['data' => $details, 'file_path' => $file_path], function ($msg) use ($details) {
            //     $msg->from('hello@credpal.com', 'Credpal');

            //     $msg->to($details['email'], 'Credpal')->subject('Welocome');
            // });
            return $this->sendResponse($file_path, 'Password reset link generated successfully.');
        }

    }

    public function Verifytoken($token)
    {

        $confirmed = DB::table('users')->where('user_token', $token)->first();

        if ($confirmed) {
            // The matching $token is found. So show a view to set the password
            return $this->sendResponse($token, 'Token Verified.');

        } else {
            return $this->sendError('Invalid token. Please ensure your token is correct');
        }
    }
    public function viewprofile(){
        return 'all';
    }

    public function updatePassword(Request $request)
    {
        $details = $request->all();
        $validator = Validator::make($details, [
            'token' => 'required',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Please fill all required field.s', $validator->errors());
        }

        $confirmed = DB::table('users')->where('reset_token', $details['token'])->first();

        if (empty($confirmed)) {
            // The matching $token is found. So show a view to set the password
            return $this->sendError('Invalid token. Please ensure your token is correct');

        }
        $token = hash_hmac('sha256', str_random(40), config('app.key'));
        $password = bcrypt($details['c_password']);

        $saved = DB::table('users')
            ->where('reset_token', $details['token'])
            ->update([
                'password' => $password,
                'reset_token' => $token
            ]);
        if ($saved) {
            // The matching $token is found. So show a view to set the password
            return $this->sendResponse('Password Changed', 'Password successfuly changed.');

        } else {

            return $this->sendError('Password not updated.', $validator->errors());

        }

    }

}