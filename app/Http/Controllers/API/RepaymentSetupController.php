<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\UserProfile;
use App\Loan;
use App\LoanRepayment;
use Validator;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Classes\LoanCalculator;
use App\Classes\CreditBureauEngine;

class RepaymentSetupController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created orders in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function setup(Request $request)
    {
        $rate = env('LOAN_RATE');

        $details = $request->all();

        $validator = Validator::make($details, [
            'tenure_month' => 'required',
            'amount' => 'required',
            'email' => 'required'
        ]);

        $breakdown = new LoanCalculator();

        $getresult = $breakdown->averagereducingbalance($details['amount'], $rate, $details['tenure_month']);

      
          

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }



        $user = DB::table('users')->where('email', $details['email'])->value('id');
        $getloans = DB::table('loans')->where('user_id', $user)->value('id');
        $order_id = DB::table('order')->where('order_user_id', 24)->value('order_id');
    
        for ($i = 0; $i < $details['tenure_month']; $i++) {

            $exist = DB::table('repayment_setup')->where([['rs_order_id', '=', Input::get('order_id')], ['rs_date', '=', Input::get('order_date')],])->first();

            if (!$exist) {
                $insertprofile = DB::table('repayment_setup')->insert(array(
                    'rs_order_id' => $order_id,
                    'rs_months' => $details['number_of_months'],
                    'rs_user_id' => $user,
                    'rs_date' => strtotime(date('Y-m-d')),
                ));

            }
        }


        return $this->sendResponse($details, 'Repayment setup was created successfully.');
    }


}