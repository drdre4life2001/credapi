<?php


namespace App\Http\Controllers\API;

use Spatie\PdfToText\Pdf;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Order;
use App\Repayment;
use App\Classes\LoanCalculator;
use Validator;
use DB;
use Illuminate\Support\Facades\Auth;

class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Store a newly created orders in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        
        $rate = env('LOAN_RATE');

        $details = $request->all();

        $validator = Validator::make($details, [
            'order_description' => 'required',
            'order_price' => 'required',
            'order_lender_id' => 'required',
            'order_source' => 'required',
            'order_drop' => 'required',
            'order_date' => 'required',
            'order_merchant_id' => 'required',
            'order_status' => 'required',
            'order_equity' => 'required',
            'order_email' => 'required',
            'tenure' =>'required'
        ]);

        $order_hash = uniqid();

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
       
        $userid = DB::table('users')->where('email', $details['order_email'])->value('id');
        if (empty($userid)) {
            return $this->sendError('Not Found.', 'Eamil does not exist');
        }
       
        $credit = DB::table('user_profile')->where('up_user_id', $userid)->value('up_credit');
        
        //check if price is greater than credit limit
        ($details['order_price'] > $credit)? $equity = $details['order_price'] - $credit: $equity =0;
      
        $breakdown = new LoanCalculator();

        $monthly_payment = $breakdown->creditformular($details['order_price'], $rate, $details['tenure']);
        $rsdate = strtotime($details['order_date']);
       
        $data = array(
            'order_user_id' =>$userid,
            'order_description' =>$details['order_description'],
            'order_price' => $details['order_price'],
            'order_lender_id' =>$details['order_lender_id'],
            'order_source' =>$details['order_source'],
            'order_drop' =>$details['order_drop'],
            'order_date' =>$rsdate,
            'order_equity' =>$equity,
            'order_merchant_id' => $details['order_merchant_id'],
            'order_status' => $details['order_status'],
            'order_equity' =>$details['order_equity'],
            'order_hash' =>$order_hash
        );

       

        $order = Order::create($data);
        
        $order_id = DB::table('order')->where('order_user_id', $userid)->value('order_id');
        
        if (empty($order_id)) {
            return $this->sendError('Not Found.', 'Order was not found');
        }
        
        $exist = DB::table('repayment_setup')->where([['rs_order_id', '=', $order_id], ['rs_date', '=', $rsdate]])->first();
        if (!$exist) {
            
            $insertprofile = DB::table('repayment_setup')->insert(array(
                'rs_order_id' => $order_id,
                'rs_months' => $details['tenure'],
                'rs_user_id' => $userid,
                'rs_date' => strtotime(date('Y-m-d')),
            ));
            
        }
        //repayment log setup
        $date = strtotime(date('Y-m-d'));
        for ($i = 1; $i <= $details['tenure']; $i++) {
            $repayment =DB::table('repayment_log')->insert(array(
                'rl_order_id' => $order_id,
                'rl_title' => 'month'.$i,
                'rl_amount' => $monthly_payment,
                'rl_status' => 0,
                'rl_due_date' => date('Y-m-d',strtotime("+$i month", $date)),
            ));
            
        }
        return $this->sendResponse($order, 'Order was created and repayment was setup successfully');
    }

    public function checkOrderStatus()
    {
       
    }


}