<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Order;
use Validator;
use DB;
use Illuminate\Support\Facades\Auth;

class PaymentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Store a newly created orders in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {

		$details = $request->all();
		
        $validator = Validator::make($details, [
            'transaction_ref' => 'required',
            'user_id' => 'required'
		]);
		if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
		$user_id = $details['user_id'];
		$user = DB::table('users')->where('id', $user_id)->value('id');

		if (empty($user)) {
            return $this->sendError('Not Found.', 'User was not found');
        }
    
	
	
        $result = array();
        
  //The parameter after verify/ is the transaction reference to be verified
		$url = 'https://api.paystack.co/transaction/verify/' . $details['transaction_ref'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			[
				'Authorization: Bearer sk_test_ed387414b4b93ffc3aa7251ea091787a5ffef518'
			]
		);


//    curl_setopt(
//    $ch, CURLOPT_HTTPHEADER, [
//    'Authorization: Bearer sk_live_414633379e2c96cf8206f221c2efa81c4dfa9d08']
//     );



		$request = curl_exec($ch);
		curl_close($ch);
		if ($request) $result = json_decode($request, true);
		$checkifuserhavecard = DB::table('card')->where('card_user_id', $user_id)->get();

		if(count($checkifuserhavecard) >=1){

			if (array_key_exists('data', $result) && array_key_exists('status', $result['data']) && ($result['data']['status'] === 'success')) {

				$t_id = DB::table('transaction')->where('transaction_user_id',$user_id)->update(array(
					'transaction_amount' => $result['data']['amount'] / 100,
					'transactionid' => $details['transaction_ref'],
					'transaction_time' => time(),
					'transaction_user_id' =>$user_id,
					'transaction_ref' => "Success",
					'transaction_reference' => $result['data']['reference'],
					'transaction_mobile' => $result['data']['log']['mobile'],
					'transaction_authorization_code' => $result['data']['authorization']['authorization_code'],
					'transaction_card_type' => $result['data']['authorization']['card_type'],
					'transaction_last4' => $result['data']['authorization']['last4'],
					'transaction_exp_month' => $result['data']['authorization']['exp_month'],
					'transaction_exp_year' => $result['data']['authorization']['exp_year'],
					'transaction_bank' => $result['data']['authorization']['bank'],
					'transaction_reusable' => $result['data']['authorization']['reusable'],
					'transaction_country_code' => $result['data']['authorization']['country_code'],
					'transaction_ip_address' => $result['data']['ip_address'],
					'transaction_customer_id' => $result['data']['customer']['id'],
					'transaction_customer_code' => $result['data']['customer']['customer_code'],
					'transaction_first_name' => $result['data']['customer']['first_name'],
					'transaction_last_name' => $result['data']['customer']['last_name'],
					'transaction_email' => $result['data']['customer']['email'],
				));
	
	
	
				$cid = DB::table('customer')->where('customer_user_id',$user_id)->update(array(
					'customer_user_id' => $user_id,
					'customer_code' => $result['data']['customer']['customer_code'],
					'customer_first_name' => $result['data']['customer']['first_name'],
					'customer_last_name' => $result['data']['customer']['last_name'],
					'customer_email' => $result['data']['customer']['email'],
					'customer_created' => time(),
				));
	
				DB::table('card')->where('card_user_id',$user_id)->update(array(
					'card_user_id' => $user_id,
					'card_customer_id' => $cid,
					'card_authorization_code' => $result['data']['authorization']['authorization_code'],
					'card_type' => $result['data']['authorization']['card_type'],
					'card_last4' => $result['data']['authorization']['last4'],
					'card_exp_month' => $result['data']['authorization']['exp_month'],
					'card_exp_year' => $result['data']['authorization']['exp_year'],
					'card_bank' => $result['data']['authorization']['bank'],
					'card_reusable' => $result['data']['authorization']['reusable'],
					'card_country_code' => $result['data']['authorization']['country_code'],
					'card_created' => time(),
				));

				DB::table('users')->where('id',$user_id)->update(array(
					'paid' => 1,
				));
				
			return $this->sendResponse('Success', 'Setup was created successfully.');
			}
			else{
				
			return $this->sendError('Error.', 'Invalid input');
			}

		}else{
    
		if (array_key_exists('data', $result) && array_key_exists('status', $result['data']) && ($result['data']['status'] === 'success')) {

			$t_id = DB::table('transaction')->insertGetId(array(
				'transaction_amount' => $result['data']['amount'] / 100,
				'transactionid' => $details['transaction_ref'],
				'transaction_time' => time(),
				'transaction_user_id' =>$user_id,
				'transaction_ref' => "Success",
				'transaction_reference' => $result['data']['reference'],
				'transaction_mobile' => $result['data']['log']['mobile'],
				'transaction_authorization_code' => $result['data']['authorization']['authorization_code'],
				'transaction_card_type' => $result['data']['authorization']['card_type'],
				'transaction_last4' => $result['data']['authorization']['last4'],
				'transaction_exp_month' => $result['data']['authorization']['exp_month'],
				'transaction_exp_year' => $result['data']['authorization']['exp_year'],
				'transaction_bank' => $result['data']['authorization']['bank'],
				'transaction_reusable' => $result['data']['authorization']['reusable'],
				'transaction_country_code' => $result['data']['authorization']['country_code'],
				'transaction_ip_address' => $result['data']['ip_address'],
				'transaction_customer_id' => $result['data']['customer']['id'],
				'transaction_customer_code' => $result['data']['customer']['customer_code'],
				'transaction_first_name' => $result['data']['customer']['first_name'],
				'transaction_last_name' => $result['data']['customer']['last_name'],
				'transaction_email' => $result['data']['customer']['email'],
			));
			$cid = DB::table('customer')->insertGetId(array(
				'customer_user_id' => $user_id,
				'customer_code' => $result['data']['customer']['customer_code'],
				'customer_first_name' => $result['data']['customer']['first_name'],
				'customer_last_name' => $result['data']['customer']['last_name'],
				'customer_email' => $result['data']['customer']['email'],
				'customer_created' => time(),
			));

			DB::table('card')->insert(array(
				'card_user_id' => $user_id,
				'card_customer_id' => $cid,
				'card_authorization_code' => $result['data']['authorization']['authorization_code'],
				'card_type' => $result['data']['authorization']['card_type'],
				'card_last4' => $result['data']['authorization']['last4'],
				'card_exp_month' => $result['data']['authorization']['exp_month'],
				'card_exp_year' => $result['data']['authorization']['exp_year'],
				'card_bank' => $result['data']['authorization']['bank'],
				'card_reusable' => $result['data']['authorization']['reusable'],
				'card_country_code' => $result['data']['authorization']['country_code'],
				'card_created' => time(),
			));
			DB::table('users')->where('id',$user_id)->update(array(
				'paid' => 1,
			));
			return $this->sendResponse('Success', 'Setup was created successfully.');
		}
		else{
			return $this->sendError('Error.', 'Invalid input');
		}
	}
    }


}