<?php

namespace App\Classes;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Validator;
use SoapClient;

class CreditBureauEngine
{
    
    
    
    public function createTicket()
    {
        
        $client = new SoapClient("https://online.firstcentralcreditbureau.com/firstcentralnigeriawebservice/FirstCentralNigeriaWebService.asmx?WSDL");
        
        $params   = array(
            "UserName" => "fehint",
            "Password" => "fehint"
        );
        $response = $client->Login($params);
        return $response->LoginResult;
    }
    
    
    
    public function getConsumerFullReport(Request $request)
    {
        
        $validation = Validator::make($request->all(), array()); //close validation
        
        //If validation fail send back the Input with errors
        if ($validation->fails()) {
            return json_encode(array(
                "status" => "fail",
                "code" => "99"
            ));
        } else {
           
            $data = $this->consumerMatch($request);
         
            if ($data["status"] == "success") {
                $client = new SoapClient("https://online.firstcentralcreditbureau.com/firstcentralnigeriawebservice/FirstCentralNigeriaWebService.asmx?WSDL");
                
                $params = array(
                    "DataTicket" => $data["ticket"],
                    "ConsumerID" => $data["ConsumerID"],
                    "consumerMergeList" => "",
                    "SubscriberEnquiryEngineID" => "",
                    "enquiryID" => $data["enquiryID"]
                );
                
                $response = $client->GetConsumerFullCreditReport($params);
              
                $xml = simplexml_load_string($response->GetConsumerFullCreditReportResult);
            
                if ($xml) {
                    $report = DB::table('xds_fullreport')->insertGetId(array(
                        'x_user_id' => $data["user_id"],
                       // 'x_type' => 'online',
                        'x_c_id' => $data["c_id"],
                        'ConsumerID' => $xml->SubjectList->ConsumerID,
                        'SearchOutput' => $xml->SubjectList->SearchOutput,
                        'Reference' => $xml->SubjectList->Reference,
                        'Header' => $xml->PersonalDetailsSummary->Header,
                        'ReferenceNo' => $xml->PersonalDetailsSummary->ReferenceNo,
                        'Nationality' => $xml->PersonalDetailsSummary->Nationality,
                        'NationalIDNo' => $xml->PersonalDetailsSummary->NationalIDNo,
                        'BankVerificationNo' => $xml->PersonalDetailsSummary->BankVerificationNo,
                        'PencomIDNo' => $xml->PersonalDetailsSummary->PencomIDNo,
                        'otheridNo' => $xml->PersonalDetailsSummary->otheridNo,
                        'BirthDate' => $xml->PersonalDetailsSummary->BirthDate,
                        'Dependants' => $xml->PersonalDetailsSummary->Dependants,
                        'Gender' => $xml->PersonalDetailsSummary->Gender,
                        'ResidentialAddress1' => $xml->PersonalDetailsSummary->ResidentialAddress1,
                        'ResidentialAddress2' => $xml->PersonalDetailsSummary->ResidentialAddress2,
                        'ResidentialAddress3' => $xml->PersonalDetailsSummary->ResidentialAddress3,
                        'ResidentialAddress4' => $xml->PersonalDetailsSummary->ResidentialAddress4,
                        'CellularNo' => $xml->PersonalDetailsSummary->CellularNo,
                        'EmailAddress' => $xml->PersonalDetailsSummary->EmailAddress,
                        'PropertyOwnedType' => $xml->PersonalDetailsSummary->PropertyOwnedType,
                        'Surname' => $xml->PersonalDetailsSummary->Surname,
                        'FirstName' => $xml->PersonalDetailsSummary->FirstName,
                        'TotalMonthlyInstalment' => $xml->CreditAccountSummary->TotalMonthlyInstalment,
                        'TotalOutstandingdebt' => $xml->CreditAccountSummary->TotalOutstandingdebt,
                        'TotalAccountarrear' => $xml->CreditAccountSummary->TotalAccountarrear,
                        'Amountarrear' => $xml->CreditAccountSummary->Amountarrear,
                        'TotalAccounts' => $xml->CreditAccountSummary->TotalAccounts,
                        'TotalAccounts1' => $xml->CreditAccountSummary->TotalAccounts1,
                        'TotalaccountinGodcondition' => $xml->CreditAccountSummary->TotalaccountinGodcondition,
                        'TotalNumberofJudgement' => $xml->CreditAccountSummary->TotalNumberofJudgement,
                        'TotalJudgementAmount' => $xml->CreditAccountSummary->TotalJudgementAmount,
                        'LastJudgementDate' => $xml->CreditAccountSummary->LastJudgementDate,
                        'TotalNumberofDishonoured' => $xml->CreditAccountSummary->TotalNumberofDishonoured,
                        'TotalDishonouredAmount' => $xml->CreditAccountSummary->TotalDishonouredAmount,
                        'TotalMonthlyInstalment1' => $xml->CreditAccountSummary->TotalMonthlyInstalment1,
                        'TotalOutstandingdebt1' => $xml->CreditAccountSummary->TotalOutstandingdebt1,
                        'TotalAccountarrear1' => $xml->CreditAccountSummary->TotalAccountarrear1,
                        'Amountarrear1' => $xml->CreditAccountSummary->Amountarrear1,
                        'TotalaccountinGodcondition1' => $xml->CreditAccountSummary->TotalaccountinGodcondition1,
                        'TotalNumberofJudgement1' => $xml->CreditAccountSummary->TotalNumberofJudgement1,
                        'TotalJudgementAmount1' => $xml->CreditAccountSummary->TotalJudgementAmount1,
                        'LastJudgementDate1' => $xml->CreditAccountSummary->LastJudgementDate1,
                        'TotalNumberofDishonoured1' => $xml->CreditAccountSummary->TotalNumberofDishonoured1,
                        'TotalDishonouredAmount1' => $xml->CreditAccountSummary->TotalDishonouredAmount1,
                        'Rating' => $xml->CreditAccountSummary->Rating,
                        'NoOfHomeLoanAccountsGood' => $xml->AccountRating->NoOfHomeLoanAccountsGood,
                        'NoOfHomeLoanAccountsBad' => $xml->AccountRating->NoOfHomeLoanAccountsBad,
                        'NoOfAutoLoanccountsGood' => $xml->AccountRating->NoOfAutoLoanccountsGood,
                        'NoOfAutoLoanAccountsBad' => $xml->AccountRating->NoOfAutoLoanAccountsBad,
                        'NoOfStudyLoanAccountsGood' => $xml->AccountRating->NoOfStudyLoanAccountsGood,
                        'NoOfStudyLoanAccountsBad' => $xml->AccountRating->NoOfStudyLoanAccountsBad,
                        'NoOfPersonalLoanAccountsGood' => $xml->AccountRating->NoOfPersonalLoanAccountsGood,
                        'NoOfPersonalLoanAccountsBad' => $xml->AccountRating->NoOfPersonalLoanAccountsBad,
                        'NoOfCreditCardAccountsGood' => $xml->AccountRating->NoOfCreditCardAccountsGood,
                        'NoOfCreditCardAccountsBad' => $xml->AccountRating->NoOfCreditCardAccountsBad,
                        'NoOfRetailAccountsGood' => $xml->AccountRating->NoOfRetailAccountsGood,
                        'NoOfRetailAccountsBad' => $xml->AccountRating->NoOfRetailAccountsBad,
                        'NoOfJointLoanAccountsGood' => $xml->AccountRating->NoOfJointLoanAccountsGood,
                        'NoOfJointLoanAccountsBad' => $xml->AccountRating->NoOfJointLoanAccountsBad,
                        'NoOfTelecomAccountsGood' => $xml->AccountRating->NoOfTelecomAccountsGood,
                        'NoOfTelecomAccountsBad' => $xml->AccountRating->NoOfTelecomAccountsBad,
                        'NoOfOtherAccountsGood' => $xml->AccountRating->NoOfOtherAccountsGood,
                        'NoOfOtherAccountsBad' => $xml->AccountRating->NoOfOtherAccountsBad
                    ));
                    
                    if ($xml->CreditAgreementSummary) {
                        
                        foreach ($xml->CreditAgreementSummary as $credit) {
                            
                            DB::table('xds_credit')->insertGetId(array(
                                'xc_user_id' => $data["user_id"],
                                'xc_x_id' => $report,
                                'DateAccountOpened' => $credit->DateAccountOpened,
                                'SubscriberName' => $credit->SubscriberName,
                                'AccountNo' => $credit->AccountNo,
                                'SubAccountNo' => $credit->SubAccountNo,
                                'IndicatorDescription' => $credit->IndicatorDescription,
                                'OpeningBalanceAmt' => $credit->OpeningBalanceAmt,
                                'Currency' => $credit->Currency,
                                'CurrentBalanceAmt' => $credit->CurrentBalanceAmt,
                                'InstalmentAmount' => $credit->InstalmentAmount,
                                'ClosedDate' => $credit->ClosedDate,
                                'LoanDuration' => $credit->LoanDuration,
                                'RepaymentFrequency' => $credit->RepaymentFrequency,
                                'LastUpdatedDate' => $credit->LastUpdatedDate,
                                'PerformanceStatus' => $credit->PerformanceStatus,
                                'AccountStatus' => $credit->AccountStatus
                            ));
                            
                        }
                        
                        
                        return json_encode(array(
                            "status" => "success",
                            "code" => "00"
                        ));
                    } else {
                        return json_encode(array(
                            "status" => "success",
                            "code" => "22"
                        ));
                    }
                } else {
                    return json_encode(array(
                        "status" => "fail",
                        "code" => "99"
                    ));
                }
                
            } else {
                return json_encode(array(
                    "status" => "fail",
                    "code" => "33"
                ));
            }
        }
    }
    
    public function consumerMatch(Request $request)
    {
        
        $validation = Validator::make($request->all(), array()); //close validation
        
        //If validation fail send back the Input with errors
        if ($validation->fails()) {
            return Redirect::back()->withInput()->withErrors($validation->messages());
        } else {
            $bvn     = $request->bvn;
            $user_id = $request->user_id;
            if ($request->input('ticket'))
                $ticket = $request->ticket;
            else
                $ticket = $this->createTicket();
            
            $client = new SoapClient("https://online.firstcentralcreditbureau.com/firstcentralnigeriawebservice/FirstCentralNigeriaWebService.asmx?WSDL");
            
            $params   = array(
                "DataTicket" => $ticket,
                "EnquiryReason" => "Test",
                "ConsumerName" => "",
                "DateOfBirth" => "",
                "Identification" => $bvn,
                "AccountNumber" => "",
                "ProductID" => "45"
            );
            //print_r($params);
            $response = $client->ConnectConsumerMatch($params);
            //echo $response->ConnectConsumerMatchResult;
            $xml      = simplexml_load_string($response->ConnectConsumerMatchResult);
            //var_dump($xml->MatchedConsumer->ConsumerID); 
            //var_dump($xml);
            if ($xml->MatchedConsumer->ConsumerID > 0) {
                foreach ($xml->MatchedConsumer as $customer) {
                    $report = DB::table('creditreport')->where('c_ConsumerID', $customer->customerID)->where('c_AccountNo', $customer->AccountNo)->first();
                    if (!$report) {
                        $report_id = DB::table('creditreport')->insertGetId(array(
                            'c_DataTicket' => $ticket,
                            'c_bvn' => $bvn,
                            'c_EnquiryReason' => "Test",
                            'c_ConsumerName' => "",
                            'c_DateOfBirth' => "",
                            'c_Identification' => $bvn,
                            'c_AccountNumber' => "",
                            'c_ProductID' => 45,
                            'c_ConsumerID' => $customer->ConsumerID,
                            'c_FirstName' => $customer->FirstName,
                            'c_Surname' => $customer->Surname,
                            'c_OtherNames' => $customer->OtherNames,
                            'c_Address' => $customer->Address,
                            'c_BirthDate' => $customer->BirthDate,
                            'c_AccountNo' => $customer->AccountNo,
                            'c_DriversLicenseNo' => "",
                            'c_VoterID' => "",
                            'c_user_id' => $user_id,
                            'c_status' => 1,
                            'c_bureau' => 1,
                            'c_EnquiryEngineID' => 0,
                            'c_enquiryID' => $customer->EnquiryID,
                            'c_consumerMergeList' => "",
                            'c_MatchingEngineID' => $customer->MatchingEngineID,
                            'c_Reference' => $customer->Reference,
                            'c_MatchingRate' => $customer->MatchingRate
                        ));
                    }
                    
                }
                $data               = array();
                $data["ticket"]     = $ticket;
                $data["ConsumerID"] = $customer->ConsumerID;
                $data["enquiryID"]  = $customer->EnquiryID;
                $data["user_id"]    = $user_id;
                $data["c_id"]       = $report_id;
                $data["status"]     = "success";
                return $data;
            } else {
                $data           = array();
                $data["status"] = "fail";
                return $data;
            }
            
        }
    }
    
    public function bureauAnalysis($bvn, $id)
    {
        //Get User Profile
        if ($bvn) {
            $status             = "true";
            $closed             = 0;
            $maximum_closed     = 0;
            $late               = 0;
            $debt               = 0;
            $code               = 11;
            $data               = array();
            $data["nonperform"] = 0;
            $data["perform"]    = 0;
            $data["open"]       = 0;
            //Check if user has recent credit bureau history
            $report = DB::table('creditreport')->where('c_bvn', $bvn)->orderBy('c_created', 'desc')->first();
          
            if (!empty($report->x_id) ) {
                //echo "adess";
                if (strtotime($report->c_created) > (time() - 2592000)) {
                    
                    $reportaccount = DB::table('xds_fullreport')->where('ConsumerID', $report->c_ConsumerID)->orderBy('x_created', 'desc')->first();
                    $reportaccountcredit = DB::table('xds_credit')->where('xc_x_id', $reportaccount->x_id)->get();
                } else {
                    //Get customer report
                    $myrequest = new \Illuminate\Http\Request();
                    $myrequest->setMethod('POST');
                    $myrequest->request->add(['bvn' => $bvn]);
                    $myrequest->request->add(['user_id' => $id]);
                    $getreport = json_decode($this->getConsumerFullReport($myrequest));
                    if ($getreport["status"] == "success") {
                        $report              = DB::table('creditreport')->where('c_bvn', $bvn)->orderBy('c_created', 'desc')->first();
                        $reportaccount       = DB::table('xds_fullreport')->where('x_c_id', $report->c_id)->orderBy('x_created', 'desc')->first();
                        $reportaccountcredit = DB::table('xds_credit')->where('xc_x_id', $reportaccount->x_c_id)->get();
                    } else {
                        $status = "fail";
                    }
                }
            }
            
            else {
                //Get customer report
                $myrequest = new \Illuminate\Http\Request();
                $myrequest->setMethod('POST');
                $myrequest->request->add(['bvn' => $bvn]);
                $myrequest->request->add(['user_id' => $id]);
                $getreport = json_decode($this->getConsumerFullReport($myrequest));
                //var_dump($getreport);
                if ($getreport->status == "success") {
                    $report              = DB::table('creditreport')->where('c_bvn', $bvn)->orderBy('c_created', 'desc')->first();
                    $reportaccount       = DB::table('xds_fullreport')->where('ConsumerID', $report->c_ConsumerID)->orderBy('x_created', 'desc')->first();
                    $reportaccountcredit = DB::table('xds_credit')->where('xc_x_id', $reportaccount->x_id)->get();
                } else {
                    $status = "fail";
                }
            }
            
            if ($status == "true") {
                foreach ($reportaccountcredit as $credit) {
                   
                    //Check for non performing
                    if ($credit->PerformanceStatus <> "Performing") {
                        $code = "33";
                        $data["nonperform"]++;
                    }
                    //Check for performing loans
                    if ($credit->PerformanceStatus == "Performing") {
                        $data["perform"]++;
                    }
                    //Check for open loans
                    //Check for performing loans
                    if ($credit->AccountStatus <> "Closed" and $credit->IndicatorDescription == "Installment account") {
                        $data["open"]++;
                    }
                    //Check for completed
                    if ($credit->PerformanceStatus == "Performing" and $credit->IndicatorDescription == "Installment account") {
                        $closed = 1;
                        if (floatval(preg_replace('/[^\d.]/', '', $credit->InstalmentAmount)) > $maximum_closed)
                        {
                            $maximum_closed = floatval(preg_replace('/[^\d.]/', '', $credit->InstalmentAmount));
                            
                        }
                    }
                    
                    //Check for outstanding repayment/debt.
                    if ((strtotime(str_replace('/', '-', $credit->ClosedDate))) > time() and $credit->IndicatorDescription == "Installment account") {
                        //Check for how many more instalments
                        $duration    = $credit->LoanDuration;
                        $closedate   = strtotime(str_replace('/', '-', $credit->DateAccountOpened)) + ($duration * 2592000);
                        $months_left = ceil(($closedate - time()) / 2592000);
                        $debt += floatval(preg_replace('/[^\d.]/', '', $credit->InstalmentAmount));
                        //echo floatval(preg_replace('/[^\d.]/', '', $credit->InstalmentAmount));
                        
                    }
                    //Check for late repayments
                    if ($credit->AccountStatus == "Performing" and (strtotime(str_replace('/', '-', $credit->ClosedDate)) + 259200) < time() and $credit->AccountStatus <> "Closed" and $credit->IndicatorDescription == "Installment account") {
                        
                        $late += $credit->InstalmentAmount;
                    }
                }
                
                if ($code == "33") {
                    return json_encode(array(
                        "status" => "fail",
                        "code" => "33"
                    ));
                } else {
                    $data["loans"] = count($reportaccountcredit);
                    
                    return json_encode(array(
                        "status" => "success",
                        "maximum" => $maximum_closed,
                        "debt" => $debt,
                        "late" => $late,
                        "code" => $code,
                        "data" => $data
                    ));
                }
            } else {
                return json_encode(array(
                    "status" => "fail",
                    "code" => "22",
                    "limit" => 0
                ));
            }
            
        } else {
            //Request for BVN
            return json_encode(array(
                "status" => "fail",
                "code" => "99"
            ));
            
        }
        
        
    }
}
