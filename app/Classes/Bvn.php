<?php 

namespace App\Classes;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;


class Bvn{

public function checkBvn($bvn){

        $url = "https://api.paystack.co/bank/resolve_bvn/{$bvn}";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer sk_live_414633379e2c96cf8206f221c2efa81c4dfa9d08']
        );

        $output = curl_exec($ch);

        curl_close($ch);

        $arr = json_decode($output, true);


        return $arr;
        // if (array_key_exists('data', $arr)) {

        //     if ($arr['data']['bvn'] != $bvn) return back()->withInput()->with(array('registererror' => 'Please supply a valid BVN'));


        // } elseif (!array_key_exists('data', $arr)) {

        //     return back()->withInput()->with(array('registererror' => 'Please supply a valid BVN'));

        // }
    
}
	

}