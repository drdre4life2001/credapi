<?php 

namespace App\Classes;

use Illuminate\Http\Request;

class LoanCalculator{


	public function flatrate($principal, $rate, $month){

		$ratepercentage = $rate/100;
		$numerator = $principal + ($principal * $month * $ratepercentage);
		$final = $numerator / $month;
		return number_format(ceil($final),2);
	}

	public function reducingbalance($principal, $rate, $month){

		$const = $principal/$month;
		$ratepercentage = $rate/100;

		for ($i=0; $i < $month; $i++) { 

			$z = $i + 1;
			$description = "Repayment {$z}";

			//$interest = ($principal - ($i * ($principal/6)));
			$interest = $ratepercentage * ($principal - ($i * ($principal/$month)));
			$repayment = $principal/$month + $interest;
			// echo $description." ".$interest." ".$const." ".$repayment;
			
			echo "<tr><th scope=".'row'." width=".'15%'.">$z</th>
				  <td width=".'20%'.">$description</td>
				  <td width=".'15%'.">₦ $interest</td>
				  <td width=".'20%'.">₦ $repayment</td></tr>";

		}
	}

	public function averagereducingbalance($principal, $rate, $month){



		$const = $principal/$month;
		$ratepercentage = $rate/100;

		$average = 0;

		for ($i=0; $i < $month; $i++) { 



			//$interest = ($principal - ($i * ($principal/6)));
			$interest = $ratepercentage * ($principal - ($i * ($principal/$month)));
			$repayment = $principal/$month + $interest;
			// echo $description." ".$interest." ".$const." ".$repayment;
				  $average += $repayment;



		}


		$avr =  $average/$month;

		for ($i=0; $i < $month; $i++) { 

			$k = $i + 1;

			$description = "Repayment {$k}";
			// echo "<tr><th scope=".'row'." width=".'15%'.">$k</th>
			// 	  <td width=".'20%'.">$description</td>
			// 	  <td scope=".'row'." width=".'20%'.">₦ $avr</td></tr>";
		}
		
		return $avr;

	}


	public function creditformular($principal, $rate, $month){



		// $const = $principal/$month;
		$ratepercentage = $rate/100;

			//$interest = ($principal - ($i * ($principal/6)));
			$interest = $ratepercentage * ($principal * $month);
			$repayment = $principal + $interest;
			$monthly_repayment = $repayment/$month;
			
			return $monthly_repayment;
	}

	public function overcredit($principal, $rate, $month, $max){

		$topay = number_format($principal - ($max*$month),2);
	    $creditavailable = $max*$month;
	    $creditavailablez = number_format($max*$month, 2); 
	    $ratepercentage = $rate/100;
	    $monthlyrepayment = number_format((($creditavailable/$month)+($ratepercentage*$month*$creditavailable))/$month,2);

	    $principalz = number_format($principal,2);

	    echo "<tr>
	    		  <td>1</td>
	    		  <td>$principalz</td>
	    		  <td>1,000.00</td>
	    		  <td>$topay</td>
				  <td>$creditavailablez</td>
				  <td>$monthlyrepayment</td></tr>";




	}

	public function checkBvn($bvn){

	
			$url = "https://api.paystack.co/bank/resolve_bvn/{$bvn}";
	
			$ch = curl_init();
	
			curl_setopt($ch, CURLOPT_URL, $url);
	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
			curl_setopt($ch, CURLOPT_HTTPHEADER, [
					'Authorization: Bearer sk_live_414633379e2c96cf8206f221c2efa81c4dfa9d08']
			);
	
			$output = curl_exec($ch);
	
			curl_close($ch);
	
			$arr = json_decode($output, true);
	
	
			return $arr;
		
		
	}



}