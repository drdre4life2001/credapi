<?php 

namespace App\Classes;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class SendEmail{


	public function sendMail($sender, $receiver, $name, $message, $reply, $subject)
  {

    $mail = new PHPMailer;
    $mail->SMTPDebug = 2;
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';

    $mail->setFrom($sender, 'CredPal Team');

    //To address and name
    $mail->addAddress($receiver); //Recipient name is optional
   
    
    //Address to which recipient will reply
    $mail->addReplyTo($reply, "{$name}");
    
    //Send HTML or Plain Text email
    $mail->isHTML(true);

    $mail->Subject = $subject;

    $mail->msgHTML($message);


    $mail->send();
  }


}