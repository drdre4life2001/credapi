<?php

namespace App;



use Illuminate\Database\Eloquent\Model;

class CredpalUser extends Model
{
  
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user_profile';

    protected $fillable = [
        'up_employment_status', 'up_gender', 'up_home_status', 'up_city', 'up_lga', 'up_location', 'up_salary', 
       'up_occupation', 'up_current_job','up_loans', 'up_loans', 'up_dob', 'up_bvn','up_credit'
    ];
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}